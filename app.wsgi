activate_this = '/var/www/shaney_markov/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
sys.path.insert(0, '/var/www/shaney_markov')
sys.path.insert(0, '/var/www/shaney_markov/models')
sys.path.insert(0, '/var/www/shaney_markov/config')

from app import app as application
