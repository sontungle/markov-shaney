"""
Model for the dictionary entries for using by markov chain
"""
from flask.ext.sqlalchemy import SQLAlchemy
from app import app
import random

db = SQLAlchemy(app)

class Category(db.Model):
    """
    Class to represent a category
    which an entry belong to
    """
    __tablename__ = 'categories'
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(256))
    entries = db.relationship('Entry', backref='category', lazy='dynamic')

    def __init__(self, label):
        """
        Create new category instance with given label
        """
        self.label = label

    @staticmethod
    def create_new(label):
        """
        Save new category with label
        """
        new_category = Category.query.filter_by(label=label).first()
        if not new_category:
            new_category = Category(label)
            db.session.add(new_category)
            db.session.commit()

    @staticmethod
    def get_all():
        """
        Get all the existing category
        """
        return Category.query.all()

    def get_id(self):
        """
        Return its ID
        """
        return self.id

    def get_label(self):
        """
        Return its label
        """
        return self.label

class Entry(db.Model):
    """
    Class to represent shaney dictionary entry
    With prefix contains two words
    and postfix contains the following word
    """
    __tablename__ = 'entries'
    prefix = db.Column(db.String(128), primary_key=True, index=True)
    postfix = db.Column(db.String(64), primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'), primary_key=True, index=True)

    @staticmethod
    def create_new(a_string, category_id):
        """
        Create new entry with give prefix and postfix
        if not already exist in database
        """
        a_string = a_string.split(' ')
        prefix = ' '.join(a_string[:2])
        postfix = a_string[-1]
        entry = Entry(prefix, postfix, category_id)
        db.session.merge(entry)
        db.session.commit()

    @staticmethod
    def select_random_postfix(prefix, category_id):
        """
        Select random postfix with given prefix
        If no entry with that prefix found
        return empty string
        """
        value = ''
        entries = Entry.query.filter_by(prefix=prefix, category_id=category_id).all()
        if entries != []:
            entry = entries[random.randrange(0, len(entries))]
            value = entry.get_postfix()
        return value

    @staticmethod
    def select_random_prefix(category_id):
        """
        Select prefix of an
        random entry from database
        If no record in database return empty string
        """
        value = ''
        entries = Entry.query.filter_by(category_id=category_id).all()
        if entries != []:
            entry = entries[random.randrange(0, len(entries))]
            value = entry.get_prefix()
        return value

    def __init__(self, prefix, postfix, category_id):
        """
        Create new entry object with given
        prefix and postfix
        """
        self.prefix = prefix
        self.postfix = postfix
        self.category_id = category_id

    def __str__(self):
        """
        Representing itself with prefix and postfis
        """
        return self.get_prefix() + '|' + self.get_postfix()

    def get_prefix(self):
        """
        Return the entry prefix
        """
        return self.prefix

    def get_postfix(self):
        """
        Return the entry postfix
        """
        return self.postfix
