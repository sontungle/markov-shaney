"""
Main config file for the app
"""

# General
DEBUG = True

# Database connection
SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/shaney_markov'
