#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Bootstrap file for the application
And handle routing too
"""

# Add folder to sys path
import sys
sys.path.append('models')

# import needed modules
from flask import Flask, render_template, request
import re

# Init the app
app = Flask(__name__)
app.config.from_pyfile('config/main_cfg.py')

import entry

# Route processing
@app.route('/', methods=['POST', 'GET'])
def index():
    # init some of the variables
    text, gen_text, errors = None, None, None
    splitter = re.compile(r'(\S+)')

    # Get all categories in the db
    categories = entry.Category.get_all()

    entry_category = ''

    # When user submit any form
    if request.method == 'POST':
        entry_category = request.form['categories']
        # When user submit training text
        if request.form['submit_btn'] == 'Input training text':
            text = request.form['training_text']
            text = splitter.findall(text)
            training_set = set([])

            for index in range(len(text) - 2):
                    training_set.add(' '.join(text[index:index + 3]))

            for a_string in training_set:
                entry.Entry.create_new(a_string, entry_category)
        # When user generates text
        elif request.form['submit_btn'] == 'Generate Text!':
            pretext = request.form['pretext'].split(' ')
            if len(pretext) >= 2:
                prefix = ' '.join(pretext[-2:])
                gen_text = pretext
            else:
                prefix = entry.Entry.select_random_prefix(entry_category)
                gen_text = prefix
            while True:
                next = entry.Entry.select_random_postfix(prefix, entry_category)
                last = prefix.split(' ')[-1]
                if next == '' or len(gen_text) > 2000:
                    break
                gen_text += ' ' + next
                prefix = ' '.join([last, next])
        # When user create category
        elif  request.form['submit_btn'] == 'Create Category':
            new_category = request.form['category']
            if new_category != '':
                entry.Category.create_new(new_category)
            # Reset the categories
            categories = entry.Category.get_all()

    return render_template('index.html', training_text=text, gen_text=gen_text, categories=categories, errors = errors, entry_category=entry_category)


if __name__ == '__main__':
    app.run()
